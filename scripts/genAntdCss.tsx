// Script to make SSR work with Antd + Next.js 13
// This will generate a full antd.min.css file directly in the specified directory (e.g. ./public)
// Source: https://github.com/ant-design/create-next-app-antd
import fs from 'fs';
import { extractStyle } from '@ant-design/static-style-extract';
import MixedTheme from '../themes/MixedTheme';

const outputPath = './public/antd.min.css';

// 1. default theme

//const css = extractStyle();

// 2. With custom theme

const css = extractStyle(MixedTheme);

fs.writeFileSync(outputPath, css);

console.log(css);

