import { NextPage, GetStaticProps } from 'next';
import { LayoutContent } from '@/components/layouts'
import { PokemonList, PokemonGQLResponse, PokemonStats, PokemonCatch } from '@/interfaces';
import axios from 'axios';
import { PokemonListHome } from '@/components/pokemon';
import { useState,useContext,useEffect,useMemo } from 'react';

import pokeApi from '@/utils/pokeApi';
import { json } from 'stream/consumers';
import localPokemon from '@/utils/localPokemon';
import { EntriesContext } from "@/context/entries";
import Loading from './Loading';
interface Props {
  pokemons: PokemonList[];
}

const HomePage: NextPage<Props> = ({ pokemons }) => {
  const [pokemonsCatch, setPokemons] = useState<PokemonList[]>([]);
   //const [isLoading, setIsLoading] = useState(true);
  const [isLoading, setIsLoading] = useState(true);
  const {cantidadPoke} = useContext(EntriesContext)
  const [counter, setCounter] = useState(0);
  useEffect(() => {
    
    console.log(cantidadPoke)
      const prueba = async (allNumberPoke:number) => {
      await setIsLoading(true);
      setPokemons(await pokeApi.getPokemonApi(allNumberPoke))
      await setIsLoading(false);     
    }
    prueba(cantidadPoke);
  }, [cantidadPoke])
  
  return (   
    <>
    {isLoading === true ?
        <Loading /> :
      <LayoutContent title='Listado de Pokémons'>    
          <PokemonListHome pokemonAll={pokemonsCatch} />
      </LayoutContent>
    }
    </>
   
  )
}

export default HomePage;
