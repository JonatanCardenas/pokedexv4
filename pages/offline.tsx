import { useEffect, useState } from "react";
import localTheme from '@/utils/localTheme';
import { darkTheme, lightTheme  } from '@/themes';
import {Select,Typography,Space,ConfigProvider} from 'antd';
import pikachu from '../public/img/PikachuTriste.png';
import sinInternet from '../public/img/conectate-a-internet.png';
import Image from 'next/image';
import { DetalleLayout } from "@/components/layouts";

const { Title, Text } = Typography;

const Offline = () => {

  return (
    <>
    <DetalleLayout title={''}>
      <section style={{ textAlign: 'center', marginTop: 48, marginBottom: 40 }}>
        <Space align='start'>  
          <Image src= {sinInternet} alt= 'sinInternet' style={{width:'100%', height:'auto'}}/>
        </Space>
        </section>
        <section style={{ textAlign: 'center', marginTop: 48, marginBottom: 40}}>
        <Space align='start'>        
          <Title level={2} style={{ marginBottom: 0}}>Conéctate a Internet</Title>  
        </Space>          
      </section >  
      <section style={{ textAlign: 'center', marginTop: 48, marginBottom: 40}}>
        <Space align='start'>        
          <Title level={3} style={{ marginBottom: 0}}>No se encontraron los recursos para visualizar esta página</Title>  
        </Space>          
      </section >  
       <section style={{ textAlign: 'center', marginTop: 48, marginBottom: 40 }}>
       <Space align='start'> 
          <Image  className="avatar" src= {pikachu} alt= 'pikachu' style={{width:'40%', height:'auto'}}/>
       </Space>
      </section>
    </DetalleLayout>
    </>

  )
}


export default Offline;
