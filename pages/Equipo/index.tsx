import { useEffect, useState,useContext } from 'react';
import { LayoutContent } from '@/components/layouts'
import { PokemonList } from '@/interfaces';
import { NoFavorites } from '@/components/ui';
import localPokemon from '@/utils/localPokemon';
import { PokemonListHome } from '@/components/pokemon';
import { EntriesContext } from '@/context/entries';

interface Props {
  pokemonsCaptured: PokemonList[];
}

 function MyPokemons() {

  const [pokemonsCatch, setPokemons] = useState<PokemonList[]>([]);
  const {cantidadPoke} = useContext(EntriesContext)
  useEffect(() => {
    const agregarValor = async () => {
      setPokemons(await localPokemon.pokemonsCaptured(cantidadPoke));
    }
 agregarValor();
  }, [])


  console.log('pokemonsCatch',pokemonsCatch);

  return( 

   <>
    <LayoutContent title='Listado de Pokémons'>
      {
        pokemonsCatch !== null ?
        pokemonsCatch.length == 0 ? <NoFavorites /> 
        :(
            <PokemonListHome pokemonAll={pokemonsCatch} ></PokemonListHome>
        )
        :(
          <>
          </>
        )
      }
    </LayoutContent>
  </>

  );

}
 


export default MyPokemons;