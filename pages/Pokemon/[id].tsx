import { DetalleLayout } from "@/components/layouts";
import { PokemonList } from "@/interfaces";
import { pokemonTypesEnum } from "@/utils";

import localPokemon from "@/utils/localPokemon";
import localTheme from "@/utils/localTheme";
import { ArrowLeftOutlined } from "@ant-design/icons";
import { Button, Col, Divider, Image, Progress, Row, Switch, Tag, Typography, theme } from "antd";
import Link from "next/link";
const { Title } = Typography;
import { useContext, useEffect, useRef, useState } from 'react';
import Loading from "../Loading";
import pokeApi from "@/utils/pokeApi";
import globalStyle from '../../styles/Global.module.css';
import PokemonStyle from '../Pokemon/Pokemon.module.css';
import { EntriesContext } from "@/context/entries";
const { useToken } = theme;


const initial_vacia: PokemonList = {
    name: '',
    catch: false,
    height: 12,
    id: 1,
    img: '',
    regions: [],
    stats: [],
    types: [],
    weight: 12
}

const PokemonDetalle = () => {
    const { cantidadPoke } = useContext(EntriesContext)
    const [capture, setCapture] = useState(false);
    const [pokemon, setPokemon] = useState<PokemonList>(initial_vacia);
    // const [isLoading, setIsLoading] = useState(true);
    const { isLoading, isLoadingChange } = useContext(EntriesContext);




    const tipopokemon: string = pokemon.types[0];
    const colorPokemon: string = pokemonTypesEnum[tipopokemon as keyof typeof pokemonTypesEnum]
    const { token } = useToken();

    useEffect(() => {
        import('../../public/js/push.js')
        let id: string = "";
        var input_string = window.location.href;
        id = input_string.substring(input_string.lastIndexOf('/') + 1);
        const getCachePokemon = async (id: number) => {
            await isLoadingChange(true);
            const pokemones = await pokeApi.getPokemonApi(cantidadPoke);
            setPokemon(pokemones.find(x => x.id === id)!)
            const data = await localPokemon.catchNow(id);
            setCapture(data)
            await isLoadingChange(false);

        }
        getCachePokemon(parseInt(id));

    }, [])

    return (
        (isLoading ? <Loading /> : (

            <DetalleLayout title={pokemon.name}>
                <Row gutter={[24, 24]} style={{ marginRight: '0px', background: colorPokemon }} >
                    <Col span={12}>
                        <Link href='/' passHref >
                            <Button type="text" shape='circle'
                                icon={<ArrowLeftOutlined
                                    className={globalStyle['icon-back']}
                                    style={{
                                        color: token.colorIcon,
                                        transform: 'scale(1)',
                                        padding: '5px'
                                    }} />} />
                            <Typography.Text
                                strong
                                style={{ paddingLeft: '10px' }}
                                className={PokemonStyle['text-sizes-weight']}
                            >
                                Pokedex
                            </Typography.Text >
                        </Link>
                    </Col>
                    <Col span={12} className='gutter-row' >
                        <div className={PokemonStyle['pokemon-serie']}>
                            <Typography.Text strong className={PokemonStyle['text-sizes-weight']}>#{pokemon.id < 10 ? `00${pokemon.id}` : pokemon.id < 100 ? `0${pokemon.id}` : pokemon.id}</Typography.Text >
                        </div>
                    </Col>
                </Row>
                <Row >
                    <Col
                        style={{ background: colorPokemon }}
                        className={PokemonStyle['contenedor-imagen']}
                    >
                        <Image
                            style={{ maxHeight: '200px' }}
                            src={pokemon.img}
                            alt={pokemon.name}
                        />
                    </Col>
                </Row>
                <div className={PokemonStyle['contenedor-principal']}>
                    <div className={PokemonStyle['contenedor-derecho']}>
                    </div>
                    <div className={PokemonStyle['contenedor-centro']}>
                        <Row justify={'center'}>
                            <Title >
                                {pokemon.name}
                            </Title>
                        </Row>
                        <Row justify={'center'}>
                            {
                                pokemon.types.map((type) => (
                                    <Col key={type} >
                                        <Tag color={pokemonTypesEnum[type as keyof typeof pokemonTypesEnum]} bordered style={{ width: '100px', textAlign: 'center', borderRadius: '10px' }}>
                                            {type}
                                        </Tag>
                                    </Col>
                                ))
                            }
                        </Row>
                        <Row>
                            <Col span={12}>
                                <Row >
                                    <Col span={24} className={PokemonStyle['centrar-textos']}>
                                        <Typography.Title level={3} style={{ marginBlockEnd: '0em', fontWeight: 'bolder' }}  >{pokemon.weight} KG</Typography.Title>
                                        <Typography.Title level={5} style={{ marginBlockEnd: '0em', marginBlockStart: '0em', color: 'gray' }}>Weight</Typography.Title>
                                    </Col >
                                </Row>
                            </Col>
                            <Col span={12}>
                                <Row >
                                    <Col span={24} className={PokemonStyle['centrar-textos']}>
                                        <Typography.Title level={3} style={{ marginBlockEnd: '0em', fontWeight: 'bolder' }} >{pokemon.height}  M</Typography.Title>
                                        <Typography.Title level={5} style={{ marginBlockEnd: '0em', marginBlockStart: '0em', color: 'gray' }}>Height</Typography.Title>
                                    </Col >
                                </Row>
                            </Col>
                        </Row>
                        <Row >
                            <Col span={24} className={PokemonStyle['centrar-textos']}>
                                <Typography.Title >
                                    Base Stats
                                </Typography.Title>
                            </Col>
                            {
                                pokemon.stats.filter(x => x.stat != 'speed').map((stat) => (
                                    <Col span={24} key={stat.stat}>
                                        <Row >
                                            <Col span={3} className={PokemonStyle['centrar-textos']}>
                                                <Typography.Text style={{ fontWeight: 'bold' }} >{localPokemon.onStateName(stat.stat)}</Typography.Text>
                                            </Col>
                                            <Col span={20}>
                                                <Progress percent={stat.base} strokeColor={localTheme.onColor(localPokemon.onStateName(stat.stat))} showInfo={false} strokeWidth={13} />
                                            </Col>
                                        </Row>
                                    </Col>

                                ))}
                        </Row>
                    </div>
                    <div className={PokemonStyle['contenedor-izquierdo']}>

                    </div>

                </div>


                <Divider style={{ background: 'white' }} />
                <Row style={{ padding: '10px' }} justify={'end'} >
                    <Col span={24} style={{ textAlign: 'center' }} >
                        <Typography.Title level={4} >CATCH</Typography.Title>
                        <Switch checked={capture} onChange={() => (localPokemon.setLocalPokemon(capture, pokemon, cantidadPoke), setCapture(!capture))} />
                    </Col>
                </Row>

            </DetalleLayout>
        ))



    )
}


// export const getStaticPaths: GetStaticPaths = async (ctx) => {
//     const pokemons151 = [...Array(151)].map((value, index) => `${index + 1}`);

//     return {
//         paths: pokemons151.map(id => ({
//             params: { id }
//         })),
//         fallback: false
//     }

// }
// export const getStaticProps: GetStaticProps = async ({ params }) => {
//     const { id } = params as { id: string };
//     const response = await getCacheData.getCachePokemon(parseInt(id));

//     return {
//         props: {
//             pokemon: response
//         }
//     }
// }
export default PokemonDetalle;