import * as React from 'react';
import { ListItemIcon, Box, List, ListItem } from '@mui/material';
import { AreaChartOutlined, GitlabOutlined, SettingOutlined, TeamOutlined, MenuUnfoldOutlined, CloseOutlined,DownloadOutlined } from '@ant-design/icons';
import ListItemButton from '@mui/material/ListItemButton';
import { Col, Row, Button, Drawer, Menu, theme } from 'antd';
import Image from 'next/image';
import style from './Navbar.module.css';
import pokeImage from '../../public/img/pokeapi_256.3fa72200.png';
import pokebola from '../../public/img/pokebola.png';
import Link from 'next/link';
import { ActiveLink } from './ActiveLink';
import { useState,useContext,ChangeEvent } from 'react';
import localTheme from '@/utils/localTheme';
import type { SizeType } from 'antd/es/config-provider/SizeContext';
import pokeApi from '@/utils/pokeApi';
import { EntriesContext } from '@/context/entries';
import {getPokemones} from '../../utils/descarga/index'
interface Menu {
  menu: string;
  menuIcon: JSX.Element;
  href: string;
}

export default function LayoutMenu() {
  const [size, setSize] = useState<SizeType>('large');
  const [collapsed, setCollapsed] = useState(false);
  const [width, setWidth] = useState(310);
  const { useToken } = theme;
  const { token } = useToken()
  const {cantidadPoke, addCantidadPoke} = useContext(EntriesContext)
  
  const [state, setState] = React.useState({
    left: false,
  });
  const {darkTheme} = localTheme.getLocalTheme();
  const MenuItems: Menu[] = [
    { menu: 'Pokedex', href: './', menuIcon: <GitlabOutlined /> },
    { menu: 'Mi equipo', href: './Equipo', menuIcon: <TeamOutlined /> },
    { menu: 'Estadísticas', href: './Estadisticas', menuIcon: <AreaChartOutlined /> },
    { menu: 'Configuración', href: './Configuracion', menuIcon: <SettingOutlined /> },
    /* {menu:'Acerca de',href:'/AcercaDe',menuIcon: <InfoOutlined />} */
  ];
  function cambioCantidadPoke(){
    return addCantidadPoke(1000)
  }
  const RecuperarPokemons = async()=>{
    cambioCantidadPoke()
    console.log(cantidadPoke)
    const headers = new Headers();
    headers.append('allPokemon','pokemones');
   
    const Response = await pokeApi.getPokemonApi(cantidadPoke);
    const peticion = await getPokemones();
    /* const peticion = await fetch('https://apymsa.com.mx/pokedex/api' + '/Pokemon'
    ); */
    console.log({Response})
  /*   console.log(pokemonsCapturados) */
  }
  const toggleDrawer = (anchor: 'left', open: boolean) =>
    (event: React.KeyboardEvent | React.MouseEvent) => {
      if (
        event &&
        event.type === 'keydown' &&
        ((event as React.KeyboardEvent).key === 'Tab' ||
          (event as React.KeyboardEvent).key === 'Shift')
      ) {
        return;
      }
      setCollapsed(!collapsed);
      setState({ ...state, [anchor]: open });
    };
  const list = (anchor: 'left') => (
    <Box
      role="presentation"
      onClick={toggleDrawer(anchor, false)}
      onKeyDown={toggleDrawer(anchor, false)}
    >
      <Row justify="center" align="top">
        <Col span={12} style={{ textAlign: 'start' }}>
          <Link href="/">
            <Image src={pokebola} alt='logo pokemón' style={{ width: '115px', height: 'auto', margin: '5px' }} />
          </Link>
        </Col>
      </Row>
      <List>
        {MenuItems.map((item, index) => (
          <ListItem key={item.menu} className={style.listemicon}>
            <ListItemButton>
              <ListItemIcon style={{ color: token.colorIcon }}>
                <Link href={item.href} legacyBehavior>
                  {item.menuIcon}
                </Link>
              </ListItemIcon>
              <ActiveLink key={item.href} text={item.menu} href={item.href} />
            </ListItemButton>
          </ListItem>
        ))}
        <ListItem>    
        <Button icon={<DownloadOutlined style={{color:token.colorIcon}}/>} size={size} style={{background:darkTheme === false ? '#d7705f':'grey'}} className={style.button} placeholder="Download" onClick={RecuperarPokemons}>
            <a style={{color:token.colorIcon}}>Download</a>
        </Button>
       
        </ListItem>
      </List>
    </Box>
  );
  return (
    <div className={style.container}>
      {(['left'] as const).map((anchor) => (
        <React.Fragment key={anchor} >
          <Row justify="end" align="middle" style={{ height: '100%' }}>
            <Col span={12} style={{ paddingLeft: '15px' }}>
              <Button
                size="large"
                onClick={toggleDrawer(anchor, true)}
                type="text"
                style={{ color: token.colorIcon }}
              >
                <MenuUnfoldOutlined className={style.iconmenu} />
              </Button>
            </Col>
            <Col span={12} style={{ display: 'flex', justifyContent: 'end', paddingRight: '15px' }}>
              <Image src={pokeImage} alt='logo pokemón' style={{ width: '115px', height: 'auto' }} />
            </Col>
  
          </Row>
          <Drawer
            open={state[anchor]}
            onClose={toggleDrawer(anchor, false)}
            placement='left'
            width={width}
            closeIcon={<CloseOutlined style={{ color: token.colorIcon }} />}
          >
            {list(anchor)}
          </Drawer>
        </React.Fragment>
      ))}
    </div>
  );
}
