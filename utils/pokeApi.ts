import { PokemonGQLResponse, PokemonList, PokemonStats } from "@/interfaces";
import axios from "axios";
import { Data } from '../interfaces/pokemon-gql';

const getPokemonApi = async (numeropokemon:number) => {
  const endpoint = "https://beta.pokeapi.co/graphql/v1beta";
  const headers = {
    "content-type": "application/json",
    "Authorization": "<token>"
  };
  const graphqlQuery = {
    "operationName": "MyQuery",
    "query": `query MyQuery {
        pokemon_v2_pokemon(where: {id: {_lte: ${numeropokemon}}}) {
          id
          name,
          weight,
          height,
          pokemon_v2_encounters(distinct_on: location_area_id) {
            pokemon_v2_locationarea {
              pokemon_v2_location {
                pokemon_v2_region {
                  name
                  id
                }
              }
            }
            location_area_id
          }
          pokemon_v2_pokemonstats {
            pokemon_v2_stat {
              name
            }
            base_stat
          }
          pokemon_v2_pokemontypes {
            pokemon_v2_type {
              name
            }
          }
        }
      }
      `,
    "variables": {}
  };

//consulta con axios 
  const response: PokemonGQLResponse = await axios({
    url: endpoint,
    method: 'post',
    timeout: 100000,
    headers: headers,
    data: graphqlQuery
  }).then(({ data }) => {
    return data

  })
  
//consulta con fetch
  // const response: PokemonGQLResponse = await fetch(endpoint,{
  //   method: 'POST',
  //   headers:{
  //     'Content-Type': 'application/json'
  //   },
  //   body:JSON.stringify(graphqlQuery)
  // }).then((response) => 
  //   response.json()
  // ).then((result) => result)

  const pokemons: PokemonList[] = response.data?.pokemon_v2_pokemon?.map((poke) => ({

    id: poke.id,
    name: poke.name,
    img: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/${poke.id}.svg`,
    types: poke.pokemon_v2_pokemontypes.map((tipos) => (tipos.pokemon_v2_type.name.toString())),
    regions: poke.pokemon_v2_encounters.map((regiones) => (regiones.pokemon_v2_locationarea.pokemon_v2_location.pokemon_v2_region.name)),
    stats: poke.pokemon_v2_pokemonstats.map<PokemonStats>((stats) => ({ stat: stats.pokemon_v2_stat.name, base: stats.base_stat })),
    weight: poke.height,
    height: poke.weight,
    catch: false
  }))
  console.log(pokemons)
  return pokemons;
}

export default {
  getPokemonApi
}