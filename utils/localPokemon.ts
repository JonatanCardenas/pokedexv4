import { Pokemon, PokemonCatch, PokemonList } from "@/interfaces";
import { pokemonStatNameEnum } from "./pokemonStatNameEnum";
import pokeApi from "./pokeApi";


//import {getPokemonesLocal} from "../public/js/sw-db";

const urlPokedexAPI = 'https://apymsa.com.mx/pokedex/api';

// const urlPokedexAPI = 'https://localhost:7171/api';
const getLocalStorage = async (cantidadpoke:number): Promise<PokemonList[]> => {
    // const pokemonCatch = localStorage.getItem(itemLocalStorage);

    
    const pokemonCatchBD: PokemonCatch[] = await fetch(urlPokedexAPI + '/Pokemon/').then(resp => resp.json());
    const pokemonesListado = await pokeApi.getPokemonApi(cantidadpoke);
    console.log({ pokemonCatchBD });
    // console.log({ pokemonesListado });
    const pokemonesCapturados = pokemonesListado.filter((pokemon) => pokemonCatchBD.find((pokeCatch) => pokeCatch.pokemonId === pokemon.id));
    pokemonesCapturados.map((pokemon) => {
        pokemonCatchBD.map((pokemonCatch) => {
            if (pokemon.id === pokemonCatch.pokemonId) {
                pokemon.catch = pokemonCatch.capturado
            }
        })
    });
    return pokemonesCapturados;
}

const pokemones = async () => {
    console.log("pokemones")
    let data: PokemonCatch[] = [];
    const peticion = await fetch(urlPokedexAPI + '/Pokemon');
    data = await peticion.json();
    return data;
}

const catchNow = async (pokemonId: number): Promise<boolean> => {


    const online = navigator.onLine
    console.log({ online })
    // if (online) {
    const pokemonesC = await pokemones();
    console.log({ pokemonesC });
    var elementOn = pokemonesC.filter((pokeCatch) => pokeCatch.pokemonId === pokemonId);
    if (elementOn == null) return false;
    return pokemonesC.filter((pokeCatch) => pokeCatch.pokemonId === pokemonId)[0]?.capturado;
    // }
    // else {
    //     console.log("offline");
    //     // var pokemonesL = getPokemonesLocal();
    //     // console.log(pokemonesL)
    //     return false;
    // }


}

const pokemonsCaptured = async (cantidadPokemons:number): Promise<(PokemonList[])> => {
    let catpureNow: PokemonList[] = await getLocalStorage(cantidadPokemons);
    let pokemonsCaptured: PokemonList[] = [];
    if (catpureNow !== null) {
        pokemonsCaptured = catpureNow.filter((poke) => poke.catch === true);
    }

    return pokemonsCaptured;

}
const getLocalStoragePokemons = (): PokemonList[] => {
    const pokemones = window.localStorage.getItem('pokemons');

    return JSON.parse(pokemones!)
}
const getLocalStoragePokemon = (id: number): PokemonList => {
    const pokemones = getLocalStoragePokemons();

    return pokemones.find(x => x.id === id)!;
}

const setLocalPokemon = async (capture: boolean, pokemon: PokemonList,cantidadPoke:number) => {
    let idCompleto = pokemon.name.substring(0, 3) + pokemon.id.toString().padStart(3, '0');
    // const pokemonMicro = await fetch(urlPokedexAPI + '/Pokemon/' + idCompleto).then(resp => resp.json());
    const pokemonesC = await pokemones();
    let pokemonExiste: Boolean = false;
    console.log({ pokemonesC });
    if (pokemonesC.length > 0) {
        pokemonExiste = pokemonesC.filter(valor => valor.pokemonId === pokemon.id).length > 0 ? true : false;
        console.log("hola", pokemonesC.filter(valor => valor.pokemonId === pokemon.id).length > 0)

    }

    // if (pokemonMicro['data'] != null) {
    //     pokemonExiste = true;
    // } else if (pokemonMicro['data'] == undefined) {

    //     if (pokemonMicro.length > 0) {
    //         type pokemonIndexdb = {
    //             capturado: boolean;
    //             _id: string;

    //         }
    //         let pokemonesIndexDB: pokemonIndexdb[] = pokemonMicro;
    //         pokemonExiste = pokemonesIndexDB.filter(valor => valor._id === idCompleto).length > 0 ? true : false;
    //         // let pokemonExistenteIndexDB = pokemonesIndexDB.find(valor => valor._id === idCompleto)?.capturado;
    //     }

    // }
    capture = !capture;

    type dataPokemon = {
        id: string,
        nombre: string,
        pokemonId: number,
        capturado: boolean
    }

    let data: dataPokemon = {
        nombre: pokemon.name,
        id: idCompleto,
        pokemonId: pokemon.id,
        capturado: capture
    }

    console.log("POKEMON_EXISTE", pokemonExiste);

    if (pokemonExiste) {
        fetch(urlPokedexAPI + '/Pokemon/' + idCompleto, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)

        })
            .then(res => {
                console.log({ res });
                return res.json()
            })
            .then(res => console.log('RESPONSE_FETCH', res))
            .catch(err => console.log('ERROR_FETCH:', err));
    } else {
        fetch(urlPokedexAPI + '/Pokemon', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
            .then(res => res.json())
            .then(res => console.log('app.js', res))
            .catch(err => console.log('app.js error:', err));
    }



}
const onStateName = (stat: string) => {
    return pokemonStatNameEnum[stat as keyof typeof pokemonStatNameEnum]
}
const getPokemonListById = (): PokemonList => {
    let id: string = "";


    var input_string = window.location.href;
    id = input_string.substring(input_string.lastIndexOf('/') + 1);
    const pokemon = getLocalStoragePokemon(parseInt(id));
    return pokemon;
}

export default {
    getLocalStorage,
    catchNow,
    pokemonsCaptured,
    getLocalStoragePokemon,
    setLocalPokemon,
    onStateName,
    getPokemonListById,
    pokemones
}