function manageApiPokemons(cacheName, req) {
    //console.log("cacheName", cacheName, req);
    if (req.clone().method === 'POST') {
        if (self.registration.sync) {
            return req.clone().text().then(body => {

                console.log(body);
                const bodyObj = JSON.parse(body);
                return savePokemon(bodyObj);
            });
        } else {
            return fetch(req);
        }
    } else if (req.clone().method === 'PUT') {
        //Actualizar el registro de pokemon 
        if (self.registration.sync) {
            return req.clone().text().then(body => {
                console.log(body);
                const bodyObj = JSON.parse(body);
                return updatePokemonLocal(bodyObj, "UPDATE");
            });
        } else {
            return fetch(req);
        }
    } /* else if (req.clone().method === 'GET') {
        if(req.clone().url.includes('api/Pokemon')){
        console.log(req)
            return getPokemones()
        }
    } */
}
async function pokemonsCatchs(cacheName, req) {
    return await getPokemonesLocal();
}