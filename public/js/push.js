const urlsafeBase64 = require('urlsafe-base64');
const urlPokedexAPI = 'https://apymsa.com.mx/pokedex/api';
 //const urlPokedexAPI = 'https://localhost:7171/api';

async function notificarme(){
    console.log("Entro a notificaciones")
   console.log(Notification.permission)
    if(Notification.permission === 'granted')
    {
    //   new Notification("Hola Mundo - granted")
       //registerPushNotifications()
    }
    else if (Notification.permission !== 'denied' || Notification.permission === 'default'){
        
      Notification.requestPermission( function(permission){
        
        console.log("Pregunta",permission);

        if(permission === 'granted')
        {
          new Notification("Notificaciones Activadas")
          registerPushNotifications()

        }
      })
    }
  }

async function registerPushNotifications() {
  if ('serviceWorker' in navigator && 'PushManager' in window) {
    const registration = await navigator.serviceWorker.register('/sw.js')
    const applicationServerPublicKey =  await getPublicKey();
    console.log("key" , applicationServerPublicKey)

    const subscription = await registration.pushManager.subscribe({
      userVisibleOnly: true,
      applicationServerKey: applicationServerPublicKey,
    }).catch(error => console.log(error));

    console.log(JSON.stringify(subscription))
    fetch(urlPokedexAPI + "/Notifications",{
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify(subscription)
    }).then(console.log()).catch(error => console.log(error))


    // Send the subscription details to your server for saving

    console.log('Push notifications registered:', subscription)
  } else {
    console.warn('Push notifications are not supported')
  }
}

const getPublicKey = async () => {
    console.log("obtener key ")
    return await fetch(urlPokedexAPI+"/Notifications").then(res => res.json()).then(resp => {return urlsafeBase64.decode(resp.data) });
    
  }


 notificarme()
