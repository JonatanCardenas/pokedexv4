import { FC, PropsWithChildren, useReducer } from "react";
import { entriesReducer } from "./entriesReducer";
import { EntriesContext } from "./EntriesContext";


export interface UIState {
    sidemenuOpen: boolean;
    isAddingEntry: boolean;
    isDragging: boolean;
}

export interface EntriesState {
    filters: filter[];
    search: string;
    cantidadPoke : number;
    isLoading: boolean;

}

export interface filter {
    name: string;
    value: boolean;
    filter: string;
  

}
const Entries_INITIAL_STATE: EntriesState = {
    filters: [
    ],
    search: "",
    cantidadPoke: 151,
    isLoading: true

}
export const EntriesProvider: FC<PropsWithChildren> = ({ children }) => {

    const [state, dispatch] = useReducer(entriesReducer, Entries_INITIAL_STATE);

    const addFilters = async (filtro: filter[]) => {

        dispatch({ type: 'UI - Agregar filter', payload: filtro });
    }
    const addSearch =async (search:string) => {
        dispatch({type:'UI - Agregar search',payload:search})
        
    }
    const addCantidadPoke =async (cantidadPoke:number) => {
        dispatch({type:'UI - Agregar cantidadpoke',payload:cantidadPoke})
        
    }

    const isLoadingChange = async (isLoading: boolean) => {
        dispatch({ type: 'UI - Esta cargando', payload: isLoading })

    }

    return (
        <EntriesContext.Provider value={{
            ...state,
            // Methods
            addFilters,
            addSearch,
            addCantidadPoke,
            isLoadingChange

        }}>
            {children}
        </EntriesContext.Provider>
    )
};
