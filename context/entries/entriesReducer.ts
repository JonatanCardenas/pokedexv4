import { EntriesState, filter } from "./EntriesProvider"


type UIActionType =
    | { type: 'UI - Agregar filter', payload: filter[] }
    | { type: 'UI - Agregar search', payload: string }
    | { type: 'UI - Esta cargando', payload: boolean }
    |{ type: 'UI - Agregar cantidadpoke', payload: number }


export const entriesReducer = (state: EntriesState, action: UIActionType): EntriesState => {

    switch (action.type) {
        case 'UI - Agregar filter':
            return {
                ...state,
                filters: [...action.payload],
            }
        case 'UI - Agregar search':
            return {
                ...state,
                search: action.payload
            }
        case 'UI - Agregar cantidadpoke':
                return{
                    ...state,
                    cantidadPoke: action.payload
                }
        case 'UI - Esta cargando':
            return {
                ...state,
                isLoading: action.payload
            }

        default:
            return state;
    }

}